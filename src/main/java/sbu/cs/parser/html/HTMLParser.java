package sbu.cs.parser.html;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document)
    {
        Node html = new Node(), body = new Node(), header = new Node(), para = new Node(),
        bold = new Node(), other = new Node();

        document = document.replaceAll("</*html>(\n)*", "");
        document = document.replaceAll("\n\n", "\n");

        String[] line = document.split("\n");

        for (String str : line) {
            html.Inside += str;
        }

        document = document.replaceAll("</*body>(\n)*", "");

        line = document.split("\n");

        for (String str : line) {
            body.Inside += str;
        }

        for (String str : line)
        {

            if ( str.matches(".*<h[1-6]>.*") )
            {
                str = str.replaceAll("</*h[1-6]>", "");
                header.Inside = str;

                if ( str.matches(".*<b>.*") )
                {
                    str = str.replaceFirst("\\w*<b>", "");
                    str = str.replaceAll("</b>\\w*", "");

                    bold.Inside = str;
                }
                header.children.add(bold);
                body.children.add(header);
            }

            else if ( str.matches(".*<p>.*") )
            {
                str = str.replaceAll("</*p>", "");
                para.Inside = str;

                if ( str.matches(".*<b>.*") )
                {
                    str = str.replaceFirst("\\w*<b>", "");
                    str = str.replaceAll("</b>\\w*", "");

                    bold.Inside = str;
                }
                para.children.add(bold);
                body.children.add(para);
            }

            else if ( str.matches("<.*") )
            {
                other.Inside = null;

                str = str.replaceAll("(</\\w*\\s)|\"|>", "");

                String[] atr = str.split("\\s");

                for (String a : atr)
                {
                    String[] map = a.split("=");
                    other.attributes.put(map[0], map[1]);
                }

                body.children.add(other);
            }

        }

        html.children.add(body);

        return html;
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
