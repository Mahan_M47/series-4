package sbu.cs.parser.json;

public class JsonNull extends JsonObj
{
    JsonNull (String key) {
        this.key = key;
    }

    @Override
    public String getNull () {
        return null;
    }

    public static boolean isNull (String str)
    {
        return str.equals("null");
    }
}
