package sbu.cs.parser.json;

public class JsonString extends JsonObj
{
    private final String str;

    JsonString (String key, String str) {
        str = str.replaceAll("\"","");
        this.str = str;
        this.key = key;
    }

    @Override
    public String getStr () {
        return this.str;
    }

    public static boolean isString (String str)
    {
        boolean flag = false;

        if (str.matches("\\w+")) {
            flag = true;
        }

        return flag;
    }
}
