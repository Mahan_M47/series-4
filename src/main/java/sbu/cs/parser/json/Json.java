package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {

    private final List<JsonObj> ObjList;

    public Json() {
        ObjList  = new ArrayList<>();
    }


    public void addJsonNum  (JsonNumber JNum) { this.ObjList.add(JNum); }
    public void addJsonStr  (JsonString JStr) {
        this.ObjList.add(JStr);
    }
    public void addJsonBool (JsonBoolean JBool) {
        this.ObjList.add(JBool);
    }
    public void addJsonNull (JsonNull JNull) { this.ObjList.add(JNull); }
    public void addJsonArr  (JsonArray JArr) {
        this.ObjList.add(JArr);
    }


    @Override
    public String getStringValue(String key)
    {
        for (JsonObj value : ObjList)
        {
            if ( value.checkKey(key) )
            {
                if (value instanceof JsonNumber) {
                    return value.getNum().toString();
                }

                if (value instanceof JsonString) {
                    return value.getStr();
                }

                if (value instanceof JsonBoolean) {
                    return value.getBool().toString();
                }

                if (value instanceof JsonNull) {
                    return value.getNull();
                }

                if (value instanceof JsonArray) {
                    return value.getArr();
                }
            }
        }

        return null;
    }
}
