package sbu.cs.parser.json;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data)
    {
        Json JsonObj = new Json();

        data = data.replaceAll("}|\\{|\\s|:", "");
        data = data.replaceAll(",\"","\"");
        data = data.replaceFirst("\"", "");

        String[] str = data.split("\"+");

        for (int i = 0; i < str.length; i += 2)
        {

            if ( JsonNumber.isNumber(str[i + 1]) )
            {
                JsonNumber temp = new JsonNumber(str[i], Integer.valueOf(str[i + 1]) );
                JsonObj.addJsonNum(temp);
            }
            else if ( JsonString.isString(str[i + 1]) )
            {
                JsonString temp = new JsonString(str[i], str[i + 1]);
                JsonObj.addJsonStr(temp);
            }
            else if ( JsonBoolean.isBoolean(str[i + 1]) )
            {
                JsonBoolean temp = new JsonBoolean(str[i], str[i + 1]);
                JsonObj.addJsonBool(temp);
            }
            else if ( JsonNull.isNull(str[i + 1]) )
            {
                JsonNull temp = new JsonNull(str[i]);
                JsonObj.addJsonNull(temp);

            }
            else if ( JsonArray.isArray(str[i + 1]) )
            {
                JsonArray temp = new JsonArray(str[i], str[i + 1]);
                JsonObj.addJsonArr(temp);
            }
        }

        return JsonObj;
    }


    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
