package sbu.cs.parser.json;

public class JsonBoolean extends JsonObj
{
    private final boolean bool;

    JsonBoolean (String key, String bool)
    {
        this.bool = bool.equals("true");
        this.key = key;
    }

    @Override
    public Boolean getBool () {
        return this.bool;
    }

    public static boolean isBoolean (String str)
    {
        boolean flag = false;

        if (str.equals("true") || str.equals("false")) {
            flag = true;
        }

        return flag;
    }

}
