package sbu.cs.parser.json;

public class JsonNumber extends JsonObj
{
    private final Integer num;

    JsonNumber (String key, Integer num) {
        this.num = num;
        this.key = key;
    }

    @Override
    public Integer getNum () {
        return this.num;
    }

    public static boolean isNumber (String str)
    {
        boolean flag = false;

        if (str.matches("[0-9]+")) {
            flag = true;
        }

        return flag;
    }

}
