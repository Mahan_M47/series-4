package sbu.cs.parser.json;

public class JsonArray extends JsonObj
{
    private final Integer[] arrInt;

    JsonArray (String key, String arrString)
    {
        this.key = key;

        arrString = arrString.replaceAll("\\[|]" , "");
        String[] arr = arrString.split(",");

        arrInt = new Integer[arr.length];

        for (int i = 0; i < arr.length; i++) {
            arrInt[i] = Integer.valueOf(arr[i]);
        }

    }

    @Override
    public String getArr ()
    {
        StringBuilder arrString = new StringBuilder();
        arrString.append("[");

        for (Integer tmp : this.arrInt) {
            arrString.append(tmp).append(", ");
        }
        arrString.delete(arrString.length() - 2, arrString.length());
        arrString.append("]");

        return arrString.toString();
    }

    public static boolean isArray (String str)
    {
        boolean flag = false;

        if (str.matches("\\[\\S+]")) {
            flag = true;
        }

        return flag;
    }

}
